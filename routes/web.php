<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
        return view('welcome')->with("lang",App::getLocale());
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('course', function(){
    return view("course");
})->middleware("auth");

Route::get("mlogin", 'mLoginController@index')->name('mlogin');
Route::post("mloginCheck", 'mLoginController@Authenticate')->name('mLoginCheck');

Route::get("mlogout", 'mLoginController@mlogout')->name('mlogout');

// Route::get("agecheck", 'AgeController@index')->name("agecheck");
Route::get('agecheck', function(){
    if(Gate::allows("checkage",Auth::user())){
        return view("sitehome");
    }else{
        echo "Erişim yetkiniz yok";
    }

})->middleware("auth");

Route::get("langcheck/?", 'HomeController@langCheck')->name('langCheck');

Route::post("agechecked", 'AgeController@agecheck')->name("agechecked")->middleware("CheckAge");

Route::get("mailsend", 'PageController@index')->name("mailsend");

Route::post ("sendpost", 'PageController@sendMail')->name("sendpost");
