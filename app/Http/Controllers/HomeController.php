<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // dd(Auth::user());

        $user = Auth::user();
        
        $locale = \App::getLocale();

        return view('home',$user)->with("lang",$locale);


    }

    public function profil($arg)
    {
        return dd($arg);
    }


    public function langChange($lang){
        App::setLocale($lang);
    }
}
