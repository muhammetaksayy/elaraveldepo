<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class mLoginController extends Controller
{
    public function index(){
        return view("mLogin");        
    }

    public function Authenticate(Request $request){


        $remember = $request->has("remember") ? true : false;

        $credentials = $request->only("email","password");

       if(Auth::attempt($credentials,$remember)){
            return redirect()->intended("home");

       }else{
           echo "Kayıt Bulunamadı";
           return redirect("mlogin");
       }

   
        
    }
    
    public function mLogout(){

        Auth::logout();
        return redirect("mlogin");


    }

}
