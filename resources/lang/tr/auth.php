<?php

return [
    
    /*
    |--------------------------------------------------------------------------
    | Kimlik kontrol metinleri
    |--------------------------------------------------------------------------
    |
    | Aşağıdaki metinler kimlik doğrulama (giriş) sırasında kullanıcılara
    | gösterilebilecek mesajlardır. Bu metinleri uygulamanızın
    | gereksinimlerine göre düzenlemekte özgürsünüz.
    |
    */
    'Login' => 'Giriş',
    'Register' => 'Kayıt Ol',
    'failed' => 'Girilmiş olan kullanıcı verileri sistemdekiler ile eşleşmemektedir.',
    'throttle' => 'Çok fazla oturum açma girişiminde bulundunuz. Lütfen :seconds saniye sonra tekrar deneyiz.',
];
